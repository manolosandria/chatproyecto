﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using System.Windows.Forms;

namespace Cliente
{
    public partial class Form1 : Form
    {
        static private NetworkStream stream; //permite manejar la informacion 
        static private StreamWriter streamw; //escrribir en la informacion 
        static private StreamReader streamr; // leer la informacion 
        static private TcpClient client = new TcpClient(); // nos permitira acceder a la informacion 
        static private string nick = "unknown";

        private delegate void DAddItem(String s);
        //mandara informacion entre procesos esto se refiere al intercambio de informacion 

        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            label1.Visible = false; //false se refiere a que no aparezcan 
            pictureBox1.Visible = false;
            textBox1.Visible = false;
            button1.Visible = false;
            listBox1.Visible = true;
            textBox2.Visible = true;
            button2.Visible = true;
            pictureBox2.Visible = true;
            pictureBox3.Visible = true;

            nick = textBox1.Text; // donde el cleinte colocara su nombre de usuario 
            
            Conectar(); //va a mandar a llamar la funcion  
        }
        void Conectar()
        {
            try
            {
                client.Connect("127.0.0.1", 8000); //se conecta a la direccion ip que tiene el servidor esta asinagada la direccion
                                                   //del local host 
                if (client.Connected)
                {
                    Thread t = new Thread(Listen); //si el cliente esta conectado creamos el hilo el sirve srive por ejemplo para
                                                   //tener trabajando dos bucles while o para usar varios procesos a la vez

                    stream = client.GetStream();   //obtenemos el flujo de informacion
                    streamw = new StreamWriter(stream); //que lo escriba el cliente
                    streamr = new StreamReader(stream); //leer el flujo de informacion 

                    streamw.WriteLine(nick); //mandamos a nuestro flujo de informacion lo que es el nick 
                    streamw.Flush();  //se limpia el flujo de informacion

                    t.Start(); //iniciamos nuestro proceso que es este caso es "Listen"
                }
                else
                {
                    MessageBox.Show("EL SERVIDOR NO SE ENCUENTRA DISPONIBLE","INFORMACION",MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("EL SERVIDOR NO ESTA DISPONIBLE","INFORMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Exit();
            }
        }

        void Listen() //Escchar la informacion que nos envia el cliente y el serrvidor 
        {
            while (client.Connected)
            {
                try
                {
                    this.Invoke(new DAddItem(AddItem), streamr.ReadLine());// Aqui es donde se registrara dicha informacion 
                    //entre el cliente y el sevidor 
                    //invocamos nuestro delegado que se utiliza entre procesos en este caso el proceso es el de escuchar la 
                     //informacion que se maneja entre el cliente y el servidor y se escribira dicha informacion en el listbox

                }
                catch
                {
                    MessageBox.Show("NO SE PUEDO ESTABLECER CONEXION CON EL SERVIDOR","INFORMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Application.Exit();
                }
            }
        }

        private void AddItem(String s)
        {
            listBox1.Items.Add(s);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            streamw.WriteLine(textBox2.Text); //lo que escribe el cleinte escribira
            streamw.Flush();
            textBox2.Clear();//una vez que envie el mensaje se cleinte se limpiara el textbox
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Application.Exit(); //cerrara el programa
        }

        private void Button4_Click(object sender, EventArgs e) //se encargara de minimizar la pantalla
        {
            if(WindowState==FormWindowState.Normal)
            {
                WindowState = FormWindowState.Minimized;
            }
            else
            {
                WindowState = FormWindowState.Normal;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
